<div class="bg-dark bg-gradient w-100 vh-100 d-flex">   
    <div class="container d-flex align-items-center justify-content-center">
        <form class="bg-white align-items-center justify-content-center rounded-lg p-5 vw-50 vh-75"method="POST">
            <div class="text-center pt-1 m-2">
                <p class="fw-bold mt-3 text-uppercase h5">controle financeiro pessoal</p>
            </div>
            <div style="height:54px; width:100%; clear:both;" ><?= $error ? '<p class="note note-danger h-75">Dados de acesso incorretos.</p>' : '' ?></div>
            <div class="form-outline mb-4">
                <input type="email"  class="form-control" name="email" placeholder="Email"/>
            </div>
            <div class="form-outline mb-5">
                <input type="password" class="form-control" name="password" placeholder="Senha"/>
            </div>
            <button type="submit" class="btn btn-dark btn-block mb-3 rounded-pill hover-shadow ">
                Entrar
            </button>            
        </form>
    </div>
</div>