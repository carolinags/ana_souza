<div class="bg-dark bg-gradient w-100 vh-100 d-flex">   
    <div class="container d-flex align-items-center justify-content-center">
        <div class="bg-white align-items-center justify-content-center rounded-lg p-5 vw-50 vh-75">
            <p class="display-3 text-uppercase"><?= $page ?></p>
        </div>
    </div>
</div>