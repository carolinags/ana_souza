<nav class="navbar fixed-top navbar-expand-md navbar-dark shadow-none" >
    <a class="navbar-brand text-uppercase" href="<?= base_url()?>">controle financeiro</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
        aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
    <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">                       
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Usuario</a>
                <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="<?= base_url('usuario/cadastro')?>">Cadastro</a>
                    <a class="dropdown-item" href="<?= base_url('usuario/conta')?>">Conta Bancária</a>
                    <a class="dropdown-item" href="<?= base_url('usuario/parceiros')?>">Parceiros</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Lancamentos</a>
                <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="<?= base_url('lancamentos/pagar')?>">Contas a pagar</a>
                    <a class="dropdown-item" href="<?= base_url('lancamentos/receber')?>">Contas a receber</a>
                    <a class="dropdown-item" href="<?= base_url('lancamentos/fluxo')?>">Fluxo de caixa</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Relatorios</a>
                <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="<?= base_url('relatorios/periodo')?>">Lançamentos por período</a>
                    <a class="dropdown-item" href="<?= base_url('relatorios/mensal')?>">Resumo mensal</a>
                    <a class="dropdown-item" href="<?= base_url('relatorios/anual')?>">Resumo anual</a>
                </div>
            </li>
        </ul>
        <span class="navbar-text white-text">
        <a class="nav-link" href="<?= base_url('home/login')?>">Login <span class="sr-only">(current)</span></a>
        </span>        
    </div>
   

</nav>
