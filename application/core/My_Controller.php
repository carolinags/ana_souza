<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_Controller extends CI_Controller {
	public function show($content, $menu = true)
	{
		$html = $this->load->view('common/head', null, true);
        if($menu) $html .= $this->load->view('common/navbar', null, true);
        $html .= $content;
		$html .= $this->load->view('common/footer', null, true);
        echo $html;		
	}
}

