<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginModel extends CI_Model {

    public function verify(){

        if(sizeof($_POST) == 0) return 0;

        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $this->load->library('Login','', 'acess');
        $k = $this->acess->verify($email, $password);
        if($k){ redirect('home');
        }else{ return 1;}
        
    }
}


