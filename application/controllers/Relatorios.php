<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorios extends My_Controller {

	public function periodo(){
        $data['page'] = 'periodo';
        $html = $this->load->view('common/temp_view', $data, true);
        $this->show($html);
    }

	public function mensal(){
        $data['page'] = 'mensal';
        $html = $this->load->view('common/temp_view', $data, true);
        $this->show($html);
    }

    public function anual(){
        $data['page'] =  'anual';
        $html = $this->load->view('common/temp_view', $data, true);
        $this->show($html);
    }
}

