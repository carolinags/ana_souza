<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lancamentos extends My_Controller {

	public function pagar(){         
        $data['page'] = 'pagar';
        $html = $this->load->view('common/temp_view', $data, true); 
         $this->show($html);
    }

	public function receber(){         
        $data['page'] = 'receber';
        $html = $this->load->view('common/temp_view', $data, true); 
         $this->show($html);
    }

    public function fluxo(){         
        $data['page'] = 'fluxo';
        $html = $this->load->view('common/temp_view', $data, true); 
         $this->show($html);
    }
}

