<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends My_Controller {

	public function cadastro(){
        $data['page'] = 'cadastro';
        $html = $this->load->view('common/temp_view', $data, true);        
        $this->show($html);
    }

	public function conta(){
        $data['page'] = 'conta';
        $html = $this->load->view('common/temp_view', $data, true);        
        $this->show($html);
    }

    public function parceiros(){
        $data['page'] = 'parceiros';
        $html = $this->load->view('common/temp_view', $data, true);        
        $this->show($html);
    }
}

