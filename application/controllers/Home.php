<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends My_Controller {

	public function index(){
		$data['page'] = 'Seja bem vindo!';
        $html = $this->load->view('common/temp_view', $data, true);
        $this->show($html);
    }

    public function login()
	{

		$this->load->model('LoginModel', 'login');
		$v['error'] = $this->login->verify();
		$html = $this->load->view('access/login_form', $v, true);
		$this->show($html, false);
		
	}

}

